require('dotenv').config()

const express = require('express')
const cors  = require('cors')
const morgan  = require('morgan')
const mongoose  = require('mongoose')
const path  = require('path')

/**
 * Init App
 */
const app = express();
/**
 * database
 */
mongoose.connect(
    process.env.MONGO_URL,{
    useNewUrlParser:true
})

/**
 * Configs App
 */
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan('dev'));
app.use('/files', express.static(path.resolve(__dirname,'..','tmp','uploads')))


/**
 * Routes
 */
app.use('/api',require('./routes/posts'))
app.use('/api',require('./routes/user'))
app.use('/api',require('./routes/products'))

/**
 * Server
 */
app.listen(3020)