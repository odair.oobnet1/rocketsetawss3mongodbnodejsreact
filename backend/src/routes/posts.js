const routes = require('express').Router();
const multer = require('multer');
const multerConfig = require('../config/multer')
const v = require('node-input-validator');
const Post = require('../models/Post')

routes.get('/posts', async (req,res)=>{
    const posts = await Post.find({})

    return res.json(posts);
})

routes.post('/posts',multer(multerConfig).single("file"),async(req,res)=>{
    const {originalname:name, size, key, location:url = ''} = req.file
    
    let validator = new v( req.body, {
        name:'required||maxLength:2',
    });
 
    validator.check().then(function (matched) {
        if (!matched) {
            res.status(422).send(validator.errors);
        }
    });

    const post = await Post.create({
        name,
        size,
        key,
        url,

    })
    return res.json(post)
})

routes.delete('/posts/:id',async (req,res)=>{
    const post = await Post.findById(req.params.id)
    await post.remove()

    return res.send()
})

module.exports = routes;